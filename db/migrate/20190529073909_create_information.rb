class CreateInformation < ActiveRecord::Migration[5.2]
  def change
    create_table :information do |t|
      t.string :name
      t.string :email
      t.string :subject
      t.string :body
      t.string :attachment

      t.timestamps
    end
  end
end
